﻿using System.Windows;

namespace SocialNetwork
{
    public partial class DialogWindow : Window
    {
        #region Private Members

        private DialogWindowViewModel _vm;

        #endregion

        #region Public Properties

        public DialogWindowViewModel ViewModel
        {
            get => _vm;
            set
            {
                _vm = value;
                DataContext = _vm;
            }
        }

        #endregion

        #region Constructor

        public DialogWindow()
        {
            InitializeComponent();
        }

        #endregion
    }
}