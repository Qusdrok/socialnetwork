﻿using System.Windows;
using Dna;
using static Dna.FrameworkDI;

namespace SocialNetwork
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            Framework.Construct<DefaultFrameworkConstruction>()
                .AddFileLogger().AddSocialNetworkViewModels()
                .AddSocialNetworkServices().Build();

            Logger.LogDebugSource("Application starting...");
            DI.ApplicationVM.GoToPage(ApplicationPage.Login);
        }
    }
}