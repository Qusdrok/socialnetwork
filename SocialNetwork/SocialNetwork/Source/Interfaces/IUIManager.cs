﻿using System.Threading.Tasks;

namespace SocialNetwork
{
    public interface IUIManager
    {
        Task ShowMessage(DialogViewModel vm);
    }
}