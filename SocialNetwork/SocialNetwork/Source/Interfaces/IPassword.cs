﻿using System.Security;

namespace SocialNetwork
{
    public interface IPassword
    {
        SecureString SecurePassword { get; }
    }
}
