﻿namespace SocialNetwork
{
    public class PasswordEntryModel : PasswordEntryViewModel
    {
        #region Singleton

        public static PasswordEntryModel Instance => new PasswordEntryModel();

        #endregion

        #region Constructor

        public PasswordEntryModel()
        {
            Label = "Name";
            FakePassword = "********";
        }

        #endregion
    }
}