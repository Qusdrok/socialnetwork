﻿namespace SocialNetwork
{
    public class TextEntryModel : TextEntryViewModel
    {
        #region Singleton

        public static TextEntryModel Instance => new TextEntryModel();

        #endregion

        #region Constructor
        
        public TextEntryModel()
        {
            Label = "Name";
            OriginalText = "Luke Malpass";
            EditedText = "Editing :)";
        }

        #endregion
    }
}