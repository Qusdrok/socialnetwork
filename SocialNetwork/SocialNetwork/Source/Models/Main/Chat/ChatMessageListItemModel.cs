﻿using System;

namespace SocialNetwork
{
    public class ChatMessageListItemModel : ChatMessageListItemViewModel
    {
        #region Singleton

        public static ChatMessageListItemModel Instance => new ChatMessageListItemModel();

        #endregion

        #region Constructor

        public ChatMessageListItemModel()
        {
            Initials = "LM";
            SenderName = "Luke";
            Message = "Some design time visual text";
            ProfilePictureRGB = "3099c5";
            SentByMe = true;
            MessageSentTime = DateTimeOffset.UtcNow;
            MessageReadTime = DateTimeOffset.UtcNow.Subtract(TimeSpan.FromDays(1.3));
        }

        #endregion
    }
}