﻿namespace SocialNetwork
{
    public class SettingsModel : SettingsViewModel
    {
        #region Singleton

        public static SettingsModel Instance => new SettingsModel();

        #endregion

        #region Constructor

        public SettingsModel()
        {
            FirstName = new TextEntryViewModel { Label = "Fist Name", OriginalText = "Luke" };
            LastName = new TextEntryViewModel { Label = "Last Name", OriginalText = "Malpass" };
            Username = new TextEntryViewModel { Label = "Username", OriginalText = "luke" };
            Password = new PasswordEntryViewModel { Label = "Password", FakePassword = "********" };
            Email = new TextEntryViewModel { Label = "Email", OriginalText = "contact@angelsix.com" };
        }

        #endregion
    }
}