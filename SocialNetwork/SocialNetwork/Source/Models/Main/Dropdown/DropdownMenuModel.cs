﻿using System.Collections.Generic;

namespace SocialNetwork
{
    public class DropdownMenuModel : DropdownMenuViewModel
    {
        #region Singleton

        public static DropdownMenuModel Instance => new DropdownMenuModel();

        #endregion

        #region Private Constant

        private const string PATH_AD = "/Resources/Assets/Icons/Admin";
        private const string PATH_SN = "/Resources/Assets/Icons/SocialNetwork";
        private const string PATH_VC = "/Resources/Assets/Icons/VirtualCoin";

        private Screen screen = new Screen
        {
            Page = ApplicationPage.Login,
            ViewModel = new ApplicationViewModel()
        };

        #endregion

        #region Command

        #endregion

        #region Constructor

        public DropdownMenuModel()
        {
            Items = new List<DropdownMenuItemViewModel>
            {
                new DropdownMenuItemViewModel
                {
                    Name = "Social Network",
                    Icon = $"{PATH_SN}/social_network.png",
                    SubItem = new List<DropdownMenuItemViewModel>
                    {
                        new DropdownMenuItemViewModel
                        {
                            Name = "Facebook",
                            Icon = $"{PATH_SN}/Facebook/facebook.png",
                            SubItem = new List<DropdownMenuItemViewModel>
                            {
                                new DropdownMenuItemViewModel
                                {
                                    Name = "Tạo clone",
                                    Icon = $"{PATH_SN}/Facebook/facebook.png",
                                },
                                new DropdownMenuItemViewModel
                                {
                                    Name = "Nuôi clone",
                                    Icon = $"{PATH_SN}/Facebook/facebook.png",
                                },
                                new DropdownMenuItemViewModel
                                {
                                    Name = "Chọc bạn bè",
                                    Icon = $"{PATH_SN}/Facebook/facebook.png",
                                },
                                new DropdownMenuItemViewModel
                                {
                                    Name = "Tăng follow",
                                    Icon = $"{PATH_SN}/Facebook/facebook.png",
                                },
                                new DropdownMenuItemViewModel
                                {
                                    Name = "Buff mắt live",
                                    Icon = $"{PATH_SN}/Facebook/facebook_live.png",
                                }
                            }
                        },
                        new DropdownMenuItemViewModel
                        {
                            Name = "Twitter",
                            Icon = $"{PATH_SN}/Twitter/twitter.png",
                        },
                        new DropdownMenuItemViewModel
                        {
                            Name = "Youtube",
                            Icon = $"{PATH_SN}/Twitter/twitter.png",
                        },
                        new DropdownMenuItemViewModel
                        {
                            Name = "Gmail",
                            Icon = $"{PATH_SN}/Twitter/twitter.png",
                        },
                        new DropdownMenuItemViewModel
                        {
                            Name = "Microsoft (Hotmail)",
                            Icon = $"{PATH_SN}/Twitter/twitter.png",
                        }
                    }
                },
                new DropdownMenuItemViewModel
                {
                    Name = "Virtual Coin",
                    Icon = $"{PATH_VC}/virtual_coin.png",
                    SubItem = new List<DropdownMenuItemViewModel>
                    {
                        new DropdownMenuItemViewModel
                        {
                            Name = "BC.Game",
                            Icon = $"{PATH_VC}/BCGame/bcgame.png",
                            Screen = new Screen
                            {
                                Page = ApplicationPage.VirtualCoin,
                                ViewModel = new VirtualCoinViewModel()
                            },
                            SubItem = new List<DropdownMenuItemViewModel>
                            {
                                new DropdownMenuItemViewModel
                                {
                                    Name = "Crash",
                                    Icon = $"{PATH_VC}/BCGame/crash.png"
                                },
                                new DropdownMenuItemViewModel
                                {
                                    Name = "Crash Trenball",
                                    Icon = $"{PATH_VC}/BCGame/crash_trenball.png"
                                },
                                new DropdownMenuItemViewModel
                                {
                                    Name = "Classic Dice",
                                    Icon = $"{PATH_VC}/BCGame/classic_dice.png"
                                },
                                new DropdownMenuItemViewModel
                                {
                                    Name = "Ultimate Dice",
                                    Icon = $"{PATH_VC}/BCGame/ultimate_dice.png",
                                }
                            }
                        },
                        new DropdownMenuItemViewModel
                        {
                            Name = "999Dice",
                            Icon = $"{PATH_VC}/999Dice/999dice.png",
                            Screen = new Screen
                            {
                                Page = ApplicationPage.VirtualCoin,
                                ViewModel = new VirtualCoinViewModel()
                            }
                        }
                    }
                },
                new DropdownMenuItemViewModel
                {
                    Name = "Administrator",
                    Icon = $"{PATH_VC}/virtual_coin.png",
                    SubItem = new List<DropdownMenuItemViewModel>
                    {
                        new DropdownMenuItemViewModel
                        {
                            Name = "New User",
                            Icon = $"{PATH_VC}/BCGame/bcgame.png",
                        },
                        new DropdownMenuItemViewModel
                        {
                            Name = "Normal Captcha",
                            Icon = $"{PATH_AD}/normal_captcha.png",
                        },
                        new DropdownMenuItemViewModel
                        {
                            Name = "Recaptcha V2",
                            Icon = $"{PATH_AD}/recaptcha.png"
                        }
                    }
                }
            };
        }

        #endregion

        #region Public Method

        public Screen GetScreen(DropdownMenuItemViewModel item)
        {
            foreach (var x in Items)
            {
                if (x.SubItem.Count > 0)
                {
                    foreach (var y in x.SubItem)
                    {
                        if (y.Name.Equals(item.Name))
                        {
                            return y.Screen;
                        }
                    }
                }
            }

            return screen;
        }

        #endregion
    }
}