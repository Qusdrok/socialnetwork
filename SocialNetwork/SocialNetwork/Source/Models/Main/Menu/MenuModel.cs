﻿using System.Collections.Generic;

namespace SocialNetwork
{
    public class MenuModel : MenuViewModel
    {
        #region Singleton

        public static MenuModel Instance => new MenuModel();

        #endregion

        #region Constructor

        public MenuModel()
        {
            Items = new List<MenuItemViewModel>(new[]
            {
                new MenuItemViewModel {Type = MenuItemType.Header, Text = "Design time header..."},
                new MenuItemViewModel {Text = "Menu item 1", Icon = IconType.File},
                new MenuItemViewModel {Text = "Menu item 2", Icon = IconType.Picture},
            });
        }

        #endregion
    }
}