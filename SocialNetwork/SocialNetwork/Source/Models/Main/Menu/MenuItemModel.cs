﻿namespace SocialNetwork
{
    public class MenuItemModel : MenuItemViewModel
    {
        #region Singleton

        public static MenuItemModel Instance => new MenuItemModel();

        #endregion

        #region Constructor

        public MenuItemModel()
        {
            Text = "Hello World";
            Icon = IconType.File;
        }

        #endregion
    }
}