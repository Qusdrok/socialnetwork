﻿namespace SocialNetwork
{
    public class ChatAttachmentPopupMenuModel : ChatAttachmentPopupMenuViewModel
    {
        #region Singleton

        public static ChatAttachmentPopupMenuModel Instance => new ChatAttachmentPopupMenuModel();

        #endregion

        #region Constructor

        public ChatAttachmentPopupMenuModel()
        {
        }

        #endregion
    }
}