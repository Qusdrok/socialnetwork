﻿namespace SocialNetwork
{
    public class DialogModel : DialogViewModel
    {
        #region Singleton

        public static DialogModel Instance => new DialogModel();

        #endregion

        #region Constructor

        public DialogModel()
        {
            OkText = "OK";
            Message = "Design time messages are fun :)";
        }

        #endregion
    }
}