﻿namespace SocialNetwork
{
    public class AccountModel
    {
        public string Username { get; }
        public string Password { get; }
        
        public decimal Balance { get; set; }
        public decimal Bet { get; set; }
        public decimal Cashout { get; set; }
        
        public decimal Roll { get; set; }
        public decimal TotalBet { get; set; }
        public decimal TotalProfit { get; set; }
        public decimal TotalProfitWin { get; set; }
        public decimal TotalProfitLose { get; set; }

        public bool High { get; set; }
        public string Status { get; set; }

        public AccountModel(string username, string password)
        {
            Username = username;
            Password = password;
        }
    }
}