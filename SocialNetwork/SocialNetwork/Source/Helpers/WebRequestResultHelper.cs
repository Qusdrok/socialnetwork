﻿/*using System.Threading.Tasks;
using Dna;
using SocialNetwork;
using static Dna.FrameworkDI;

namespace SocialNetworkResource
{
    public static class WebRequestResultExtensions
    {
        public static async Task<bool> HandleErrorIfFailedAsync(this WebRequestResult response, string title)
        {
            if (response?.ServerResponse != null &&
                (response.ServerResponse as ApiResponse)?.Successful != false) return false;
            var message = "Unknown error from server call";
            if (response?.ServerResponse is ApiResponse apiResponse)
            {
                message = apiResponse.ErrorMessage;
            }
            else if (!string.IsNullOrWhiteSpace(response?.RawServerResponse))
            {
                message = $"Unexpected response from server. {response.RawServerResponse}";
            }
            else if (response != null)
            {
                message = response.ErrorMessage ??
                          $"Server responded with {response.StatusDescription} ({response.StatusCode})";
            }

            if (response?.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                Logger.LogInformationSource("Logging user out due to unauthorized response from server");
                /*await ViewModelSettings.LogoutAsync();#1#
            }
            else
            {
                await DI.UI.ShowMessage(new DialogViewModel
                {
                    Title = title,
                    Message = message
                });
            }

            return true;
        }
    }
}*/