﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Dna;
using static Dna.FrameworkDI;
using System.Threading.Tasks;

namespace SocialNetwork
{
    public static class AsyncHelper
    {
        #region Private Members

        private static readonly SemaphoreSlim SelfLock = new SemaphoreSlim(1, 1);

        private static readonly Dictionary<string, SemaphoreSlim> Semaphores = new Dictionary<string, SemaphoreSlim>();

        #endregion

        public static async Task<T> AwaitResultAsync<T>(string key, Func<Task<T>> task, int maxAccessCount = 1)
        {
            #region Create Semaphore

            await SelfLock.WaitAsync();

            try
            {
                if (!Semaphores.ContainsKey(key))
                    Semaphores.Add(key, new SemaphoreSlim(maxAccessCount, maxAccessCount));
            }
            finally
            {
                SelfLock.Release();
            }

            #endregion

            var semaphore = Semaphores[key];
            await semaphore.WaitAsync();

            try
            {
                return await task();
            }
            finally
            {
                semaphore.Release();
            }
        }

        public static async Task AwaitAsync(string key, Func<Task> task, int maxAccessCount = 1)
        {
            #region Create Semaphore

            await SelfLock.WaitAsync();

            try
            {
                if (!Semaphores.ContainsKey(key))
                {
                    Semaphores.Add(key, new SemaphoreSlim(maxAccessCount, maxAccessCount));
                }
            }
            finally
            {
                SelfLock.Release();
            }

            #endregion

            var semaphore = Semaphores[key];
            await semaphore.WaitAsync();

            try
            {
                await task();
            }
            catch (Exception ex)
            {
                Logger.LogDebugSource($"Crash in {nameof(AwaitAsync)}. {ex.Message}");
                Debugger.Break();
                throw;
            }
            finally
            {
                semaphore.Release();
            }
        }
    }
}