﻿using System;
using Newtonsoft.Json;
using System.Net.Http;

namespace SocialNetwork
{
    public class Api10MinuteEmailResponse
    {
        [JsonProperty("error")] public bool Error { get; set; }

        [JsonProperty("code")] public string Code { get; set; }

        [JsonProperty("mail_get_user")] public string MailGetUser { get; set; }

        [JsonProperty("mail_get_mail")] public string MailGetMail { get; set; }

        [JsonProperty("mail_get_host")] public string MailGetHost { get; set; }

        [JsonProperty("mail_get_time")] public long MailGetTime { get; set; }

        [JsonProperty("mail_get_duetime")] public long MailGetDuetime { get; set; }

        [JsonProperty("mail_server_time")] public long MailServerTime { get; set; }

        [JsonProperty("mail_get_key")] public string MailGetKey { get; set; }

        [JsonProperty("mail_left_time")] public long MailLeftTime { get; set; }

        [JsonProperty("mail_recovering_key")] public string MailRecoveringKey { get; set; }

        [JsonProperty("mail_recovering_mail")] public string MailRecoveringMail { get; set; }

        [JsonProperty("session_id")] public string SessionId { get; set; }

        [JsonProperty("permalink")] public Permalink Permalink { get; set; }

        [JsonProperty("mail_list")] public MailList[] MailList { get; set; }
    }

    public class MailList
    {
        [JsonProperty("mail_id")] public string MailId { get; set; }

        [JsonProperty("from")] public string From { get; set; }

        [JsonProperty("subject")] public string Subject { get; set; }

        [JsonProperty("datetime")] public string Datetime { get; set; }

        [JsonProperty("datetime2")] public string Datetime2 { get; set; }

        [JsonProperty("timeago")] public long Timeago { get; set; }

        [JsonProperty("isread")] public bool IsRead { get; set; }
    }

    public class Permalink
    {
        [JsonProperty("host")] public string Host { get; set; }

        [JsonProperty("mail")] public string Mail { get; set; }

        [JsonProperty("url")] public Uri Url { get; set; }

        [JsonProperty("key")] public string Key { get; set; }

        [JsonProperty("time")] public long Time { get; set; }
    }

    public static class API10MinuteEmail
    {
        #region Constant

        private const string URL_10_MINUTE_EMAIL = "https://10minutemail.net";
        private static readonly string URLGetEmail = $"{URL_10_MINUTE_EMAIL}/address.api.php";
        private static readonly string URLCreateEmail = $"{URL_10_MINUTE_EMAIL}/address.api.php?new=1";
        private static readonly string URLReadEmail = $"{URL_10_MINUTE_EMAIL}/mail.api.php";
        private static readonly string URLReset100 = $"{URL_10_MINUTE_EMAIL}/more100.html";
        private static readonly string URLReset10 = $"{URL_10_MINUTE_EMAIL}/more.html";
        private static readonly string URLRecoverEmail = $"{URL_10_MINUTE_EMAIL}/recover.html";

        #endregion

        #region Private Member

        private static readonly HttpClient HttpClient = new HttpClient();

        #endregion

        #region Public Function

        public static Api10MinuteEmailResponse GetEmail()
        {
            var result = HttpClient.GetStringAsync(URLGetEmail).Result;
            return JsonConvert.DeserializeObject<Api10MinuteEmailResponse>(result);
        }

        public static Api10MinuteEmailResponse CreateEmail()
        {
            var result = HttpClient.GetStringAsync(URLCreateEmail).Result;
            return JsonConvert.DeserializeObject<Api10MinuteEmailResponse>(result);
        }

        public static Api10MinuteEmailResponse ReadMail(string emailID = "0")
        {
            var result = HttpClient.GetStringAsync($"{URLReadEmail}?mailid=" + emailID).Result;
            return JsonConvert.DeserializeObject<Api10MinuteEmailResponse>(result);
        }

        public static void Reset100Minutes()
        {
            HttpClient.GetStringAsync(URLReset100);
        }

        public static void Reset10Minutes()
        {
            HttpClient.GetStringAsync(URLReset10);
        }

        public static void RecoverExpiredEmail()
        {
            HttpClient.GetStringAsync(URLRecoverEmail);
        }

        #endregion
    }
}