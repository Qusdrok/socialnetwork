﻿using Dna;
using Microsoft.Extensions.DependencyInjection;

namespace SocialNetwork
{
    public static class FrameworkConstructionHelper
    {
        public static FrameworkConstruction AddSocialNetworkViewModels(this FrameworkConstruction fc)
        {
            fc.Services.AddSingleton<ApplicationViewModel>();
            fc.Services.AddSingleton<SettingsViewModel>();
            return fc;
        }

        public static FrameworkConstruction AddSocialNetworkServices(this FrameworkConstruction fc)
        {
            fc.Services.AddTransient<ITaskManager, TaskManager>();
            fc.Services.AddTransient<IFileManager, FileManager>();
            fc.Services.AddTransient<IUIManager, UIManager>();
            return fc;
        }
    }
}