﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace SocialNetwork
{
    public static class ExpressionHelpers
    {
        public static T GetPropertyValue<T>(this Expression<Func<T>> lambda)
        {
            return lambda.Compile().Invoke();
        }

        public static V GetPropertyValue<T, V>(this Expression<Func<T, V>> lambda, T input)
        {
            return lambda.Compile().Invoke(input);
        }

        public static void SetPropertyValue<T>(this Expression<Func<T>> lambda, T value)
        {
            var expression = lambda.Body as MemberExpression;
            var propertyInfo = (PropertyInfo) expression?.Member;
            var target = Expression.Lambda(expression?.Expression ?? throw new InvalidOperationException()).Compile()
                .DynamicInvoke();
            propertyInfo.SetValue(target, value);
        }

        public static void SetPropertyValue<T, V>(this Expression<Func<T, V>> lambda, V value, T input)
        {
            var expression = lambda.Body as MemberExpression;
            var propertyInfo = (PropertyInfo) expression?.Member;
            propertyInfo?.SetValue(input, value);
        }
    }
}