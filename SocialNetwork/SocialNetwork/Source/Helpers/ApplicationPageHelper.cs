﻿using System.Diagnostics;

namespace SocialNetwork
{
    public static class ApplicationPageConverters
    {
        public static BasePage ToBasePage(this ApplicationPage page, object vm = null)
        {
            switch (page)
            {
                case ApplicationPage.Login:
                    return new LoginPage(vm as LoginViewModel);

                case ApplicationPage.About:
                    return new AboutPage(vm as AboutViewModel);

                case ApplicationPage.Chat:
                    return new ChatPage(vm as ChatMessageListViewModel);

                case ApplicationPage.VirtualCoin:
                    return new VirtualCoinPage(vm as VirtualCoinViewModel);
                
                default:
                    Debugger.Break();
                    return default;
            }
        }

        public static ApplicationPage ToApplicationPage(this BasePage page)
        {
            switch (page)
            {
                case AboutPage _:
                    return ApplicationPage.About;

                case ChatPage _:
                    return ApplicationPage.Chat;

                case LoginPage _:
                    return ApplicationPage.Login;
                
                case VirtualCoinPage _:
                    return ApplicationPage.VirtualCoin;

                default:
                    Debugger.Break();
                    return default;
            }
        }
    }
}