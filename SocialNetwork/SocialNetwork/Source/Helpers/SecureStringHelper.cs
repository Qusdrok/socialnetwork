﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace SocialNetwork
{
    public static class SecureStringHelpers
    {
        public static string UnSecure(this SecureString secureString)
        {
            if (secureString == null) return string.Empty;
            var unmanagedString = IntPtr.Zero;

            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(secureString);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }
    }
}