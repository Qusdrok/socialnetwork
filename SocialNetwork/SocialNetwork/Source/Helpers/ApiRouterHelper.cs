﻿namespace SocialNetwork
{
    public static class ApiRouterHelper
    {
        #region Login / Register

        public const string Register = "api/register";
        public const string Login = "api/login";
        public const string VerifyEmail = "api/verify/email";

        #endregion

        #region User Profile

        public const string GetUserProfile = "api/user/profile";
        public const string UpdateUserProfile = "api/user/profile/update";
        public const string UpdateUserPassword = "api/user/password/update";

        #endregion

        #region Contacts

        public const string SearchUsers = "api/users/search";

        #endregion
    }
}