﻿using Dna;

namespace SocialNetwork
{
    public static class DI
    {
        public static IUIManager UI => Framework.Service<IUIManager>();
        public static IFileManager FileManager => Framework.Service<IFileManager>();
        public static ITaskManager TaskManager => Framework.Service<ITaskManager>();
        public static ApplicationViewModel ApplicationVM => Framework.Service<ApplicationViewModel>();
        public static VirtualCoinSettingViewModel VCSettingVM => Framework.Service<VirtualCoinSettingViewModel>();
    }
}