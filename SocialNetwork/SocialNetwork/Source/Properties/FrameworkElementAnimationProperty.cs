﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SocialNetwork
{
    public abstract class AnimateBaseProperty<Parent> : BaseProperty<Parent, bool>
        where Parent : BaseProperty<Parent, bool>, new()
    {
        #region Protected Properties

        protected Dictionary<WeakReference, bool> _alreadyLoaded = new Dictionary<WeakReference, bool>();
        protected Dictionary<WeakReference, bool> _firstLoadValue = new Dictionary<WeakReference, bool>();

        #endregion

        public override void OnValueUpdated(DependencyObject sender, object value)
        {
            if (!(sender is FrameworkElement element)) return;
            var alreadyLoadedReference = _alreadyLoaded.FirstOrDefault(f => f.Key.Target == sender);

            if ((bool) sender.GetValue(ValueProperty) == (bool) value && alreadyLoadedReference.Key != null) return;
            if (alreadyLoadedReference.Key == null)
            {
                var weakReference = new WeakReference(sender);
                _alreadyLoaded[weakReference] = false;
                element.Visibility = Visibility.Hidden;

                async void OnLoaded(object ss, RoutedEventArgs ee)
                {
                    element.Loaded -= OnLoaded;
                    await Task.Delay(5);
                    var firstLoadReference = _firstLoadValue.FirstOrDefault(f => Equals(f.Key.Target, sender));

                    DoAnimation(element, firstLoadReference.Key != null ? firstLoadReference.Value : (bool) value,
                        true);
                    _alreadyLoaded[weakReference] = true;
                }

                element.Loaded += OnLoaded;
            }
            else if (alreadyLoadedReference.Value == false)
            {
                _firstLoadValue[new WeakReference(sender)] = (bool) value;
            }
            else
            {
                DoAnimation(element, (bool) value, false);
            }
        }

        protected virtual void DoAnimation(FrameworkElement element, bool value, bool firstLoad)
        {
        }
    }

    public class FadeInImageOnLoadProperty : AnimateBaseProperty<FadeInImageOnLoadProperty>
    {
        public override void OnValueUpdated(DependencyObject sender, object value)
        {
            if (!(sender is Image image)) return;

            if ((bool) value)
            {
                image.TargetUpdated += Image_TargetUpdatedAsync;
            }
            else
            {
                image.TargetUpdated -= Image_TargetUpdatedAsync;
            }
        }

        private async void Image_TargetUpdatedAsync(object sender, System.Windows.Data.DataTransferEventArgs e)
        {
            await (sender as Image).FadeInAsync(false);
        }
    }

    public class AnimateSlideInFromLeftProperty : AnimateBaseProperty<AnimateSlideInFromLeftProperty>
    {
        protected override async void DoAnimation(FrameworkElement element, bool value, bool firstLoad)
        {
            if (value)
            {
                await element.SlideAndFadeInAsync(AnimationSlideInDirection.Left, firstLoad, firstLoad ? 0 : 0.3f,
                    false);
            }
            else
            {
                await element.SlideAndFadeOutAsync(AnimationSlideInDirection.Left, firstLoad ? 0 : 0.3f,
                    false);
            }
        }
    }

    public class AnimateSlideInFromRightProperty : AnimateBaseProperty<AnimateSlideInFromRightProperty>
    {
        protected override async void DoAnimation(FrameworkElement element, bool value, bool firstLoad)
        {
            if (value)
            {
                await element.SlideAndFadeInAsync(AnimationSlideInDirection.Right, firstLoad, firstLoad ? 0 : 0.3f,
                    false);
            }
            else
            {
                await element.SlideAndFadeOutAsync(AnimationSlideInDirection.Right, firstLoad ? 0 : 0.3f,
                    false);
            }
        }
    }

    public class AnimateSlideInFromRightMarginProperty : AnimateBaseProperty<AnimateSlideInFromRightMarginProperty>
    {
        protected override async void DoAnimation(FrameworkElement element, bool value, bool firstLoad)
        {
            if (value)
            {
                await element.SlideAndFadeInAsync(AnimationSlideInDirection.Right, firstLoad, firstLoad ? 0 : 0.3f);
            }
            else
            {
                await element.SlideAndFadeOutAsync(AnimationSlideInDirection.Right, firstLoad ? 0 : 0.3f);
            }
        }
    }

    public class AnimateSlideInFromTopProperty : AnimateBaseProperty<AnimateSlideInFromTopProperty>
    {
        protected override async void DoAnimation(FrameworkElement element, bool value, bool firstLoad)
        {
            if (value)
            {
                await element.SlideAndFadeInAsync(AnimationSlideInDirection.Top, firstLoad, firstLoad ? 0 : 0.3f,
                    false);
            }
            else
            {
                await element.SlideAndFadeOutAsync(AnimationSlideInDirection.Top, firstLoad ? 0 : 0.3f,
                    false);
            }
        }
    }

    public class AnimateSlideInFromBottomProperty : AnimateBaseProperty<AnimateSlideInFromBottomProperty>
    {
        protected override async void DoAnimation(FrameworkElement element, bool value, bool firstLoad)
        {
            if (value)
            {
                await element.SlideAndFadeInAsync(AnimationSlideInDirection.Bottom, firstLoad, firstLoad ? 0 : 0.3f,
                    false);
            }
            else
            {
                await element.SlideAndFadeOutAsync(AnimationSlideInDirection.Bottom, firstLoad ? 0 : 0.3f,
                    false);
            }
        }
    }

    public class AnimateSlideInFromBottomOnLoadProperty : AnimateBaseProperty<AnimateSlideInFromBottomOnLoadProperty>
    {
        protected override async void DoAnimation(FrameworkElement element, bool value, bool firstLoad)
        {
            await element.SlideAndFadeInAsync(AnimationSlideInDirection.Bottom, !value, !value ? 0 : 0.3f,
                false);
        }
    }

    public class AnimateSlideInFromBottomMarginProperty : AnimateBaseProperty<AnimateSlideInFromBottomMarginProperty>
    {
        protected override async void DoAnimation(FrameworkElement element, bool value, bool firstLoad)
        {
            if (value)
            {
                await element.SlideAndFadeInAsync(AnimationSlideInDirection.Bottom, firstLoad, firstLoad ? 0 : 0.3f);
            }
            else
            {
                await element.SlideAndFadeOutAsync(AnimationSlideInDirection.Bottom, firstLoad ? 0 : 0.3f);
            }
        }
    }

    public class AnimateFadeInProperty : AnimateBaseProperty<AnimateFadeInProperty>
    {
        protected override async void DoAnimation(FrameworkElement element, bool value, bool firstLoad)
        {
            if (value)
            {
                await element.FadeInAsync(firstLoad, firstLoad ? 0 : 0.3f);
            }
            else
            {
                await element.FadeOutAsync(firstLoad ? 0 : 0.3f);
            }
        }
    }

    public class AnimateMarqueeProperty : AnimateBaseProperty<AnimateMarqueeProperty>
    {
        protected override void DoAnimation(FrameworkElement element, bool value, bool firstLoad)
        {
            element.MarqueeAsync(firstLoad ? 0 : 3f);
        }
    }
}