﻿using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SocialNetwork
{
    public class TextEntryWidthMatcherProperty : BaseProperty<TextEntryWidthMatcherProperty, bool>
    {
        public override void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var panel = sender as Panel;
            SetWidths(panel);

            void OnLoaded(object s, RoutedEventArgs ee)
            {
                panel.Loaded -= OnLoaded;
                SetWidths(panel);

                foreach (var child in panel.Children)
                {
                    if (!(child is TextEntryControl) && !(child is PasswordEntryControl)) continue;
                    var label = child is TextEntryControl control
                        ? control.Label
                        : (child as PasswordEntryControl).Label;
                    label.SizeChanged += (ss, eee) => SetWidths(panel);
                }
            }

            if (panel != null) panel.Loaded += OnLoaded;
        }

        private static void SetWidths(Panel panel)
        {
            var maxSize = (from object child in panel.Children
                    where child is TextEntryControl || child is PasswordEntryControl
                    select child is TextEntryControl control
                        ? control.Label
                        : (child as PasswordEntryControl)?.Label
                    into label
                    select label.RenderSize.Width + label.Margin.Left + label.Margin.Right).Concat(new[] {0d})
                .Max();

            var gridLength =
                // ReSharper disable once PossibleNullReferenceException
                (GridLength) new GridLengthConverter().ConvertFromString(
                    maxSize.ToString(CultureInfo.InvariantCulture));

            foreach (var child in panel.Children)
            {
                switch (child)
                {
                    case TextEntryControl text:
                        text.LabelWidth = gridLength;
                        break;
                    case PasswordEntryControl pass:
                        pass.LabelWidth = gridLength;
                        break;
                }
            }
        }
    }
}