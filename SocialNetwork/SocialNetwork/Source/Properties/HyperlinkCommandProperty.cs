﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;

namespace SocialNetwork
{
    public class HyperlinkCommandProperty : BaseProperty<HyperlinkCommandProperty, ICommand>
    {
        public override void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(sender is Hyperlink hyperlink)) return;
            hyperlink.Click -= Hyperlink_RequestNavigate;

            if (!(e.NewValue is ICommand)) return;
            hyperlink.Click += Hyperlink_RequestNavigate;
        }

        private static void Hyperlink_RequestNavigate(object sender, RoutedEventArgs e)
        {
            var command = GetValue((DependencyObject) sender);
            command?.Execute(((Hyperlink) sender).NavigateUri);
        }
    }
}