﻿using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace SocialNetwork
{
    public class ScrollToBottomOnLoadProperty : BaseProperty<ScrollToBottomOnLoadProperty, bool>
    {
        public override void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(sender)) return;
            if (!(sender is ScrollViewer control)) return;

            control.DataContextChanged -= Control_DataContextChanged;
            control.DataContextChanged += Control_DataContextChanged;
        }

        private static void Control_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ((ScrollViewer) sender).ScrollToBottom();
        }
    }

    public class AutoScrollToBottomProperty : BaseProperty<AutoScrollToBottomProperty, bool>
    {
        public override void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(sender)) return;
            if (!(sender is ScrollViewer control)) return;

            control.ScrollChanged -= Control_ScrollChanged;
            control.ScrollChanged += Control_ScrollChanged;
        }

        private void Control_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var scroll = sender as ScrollViewer;
            Debug.Assert(scroll != null, nameof(scroll) + " != null");
            if (scroll.ScrollableHeight - scroll.VerticalOffset < 20)
                scroll.ScrollToEnd();
        }
    }
}