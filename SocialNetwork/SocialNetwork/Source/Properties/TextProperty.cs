﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace SocialNetwork
{
    public class IsFocusedProperty : BaseProperty<IsFocusedProperty, bool>
    {
        public override void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(sender is Control control)) return;
            control.Loaded += (s, se) => control.Focus();
        }
    }

    public class FocusProperty : BaseProperty<FocusProperty, bool>
    {
        public override void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(sender is Control control)) return;
            if ((bool)e.NewValue) control.Focus();
        }
    }

    public class FocusAndSelectProperty : BaseProperty<FocusAndSelectProperty, bool>
    {
        public override void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (sender is TextBoxBase control)
            {
                if ((bool)e.NewValue)
                {
                    control.Focus();
                    control.SelectAll();
                }
            }

            if (!(sender is PasswordBox password)) return;
            if (!(bool) e.NewValue) return;
            
            password.Focus();
            password.SelectAll();
        }
    }
}
