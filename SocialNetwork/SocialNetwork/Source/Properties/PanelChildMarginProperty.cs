﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace SocialNetwork
{
    public class PanelChildMarginProperty : BaseProperty<PanelChildMarginProperty, string>
    {
        public override void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var panel = sender as Panel;
            Debug.Assert(panel != null, nameof(panel) + " != null");
            panel.Loaded += (s, ee) =>
            {
                foreach (var child in panel.Children)
                    ((FrameworkElement) child).Margin =
                        (Thickness) new ThicknessConverter().ConvertFromString(e.NewValue as string);
            };
        }
    }
}