﻿using System.Windows;
using System.Windows.Controls;

namespace SocialNetwork
{
    public class NoFrameHistoryProperties : BaseProperty<NoFrameHistoryProperties, bool>
    {
        public override void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(sender is Frame frame)) return;
            frame.NavigationUIVisibility = System.Windows.Navigation.NavigationUIVisibility.Hidden;
            frame.Navigated += (ss, ee) => ((Frame) ss).NavigationService.RemoveBackEntry();
        }
    }
}