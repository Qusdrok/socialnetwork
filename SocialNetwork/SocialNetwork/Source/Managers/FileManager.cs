﻿using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace SocialNetwork
{
    public class FileManager : IFileManager
    {
        public async Task WriteTextToFileAsync(string text, string path, bool append = false)
        {
            path = NormalizePath(path);
            path = ResolvePath(path);

            await AsyncHelper.AwaitAsync(nameof(FileManager) + path, async () =>
            {
                await DI.TaskManager.Run(() =>
                {
                    using (var fileStream =
                        (TextWriter) new StreamWriter(File.Open(path, append ? FileMode.Append : FileMode.Create)))
                        fileStream.Write(text);
                });
            });
        }

        public string NormalizePath(string path)
        {
            return RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
                ? path?.Replace('/', '\\').Trim()
                : path?.Replace('\\', '/').Trim();
        }

        public string ResolvePath(string path)
        {
            return Path.GetFullPath(path);
        }
    }
}