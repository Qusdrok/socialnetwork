﻿using System.Threading.Tasks;
using System.Windows;

namespace SocialNetwork
{
    public class UIManager : IUIManager
    {
        public Task ShowMessage(DialogViewModel viewModel)
        {
            var tcs = new TaskCompletionSource<bool>();
            Application.Current.Dispatcher.Invoke(async () =>
            {
                try
                {
                    await new DialogControl().ShowDialog(viewModel);
                }
                finally
                {
                    tcs.SetResult(true);
                }
            });

            return tcs.Task;
        }
    }
}