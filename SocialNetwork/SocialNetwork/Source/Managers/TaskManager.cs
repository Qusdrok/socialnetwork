﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using static Dna.FrameworkDI;
using System.Threading.Tasks;
using Dna;

namespace SocialNetwork
{
    public class TaskManager : ITaskManager
    {
        #region Task Methods

        public async Task Run(Func<Task> function, [CallerMemberName] string origin = "",
            [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0)
        {
            try
            {
                await Task.Run(function);
            }
            catch (Exception ex)
            {
                Logger.LogErrorSource(ex.ToString(), origin: origin, filePath: filePath, lineNumber: lineNumber);
                throw;
            }
        }

        public async void RunAndForget(Func<Task> function, [CallerMemberName] string origin = "",
            [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0)
        {
            try
            {
                await Run(function, origin, filePath, lineNumber);
            }
            catch
            {
                // ignored
            }
        }

        public async Task<TResult> Run<TResult>(Func<Task<TResult>> function, CancellationToken cancellationToken,
            [CallerMemberName] string origin = "", [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0)
        {
            try
            {
                return await Task.Run(function, cancellationToken);
            }
            catch (Exception ex)
            {
                Logger.LogErrorSource(ex.ToString(), origin: origin, filePath: filePath, lineNumber: lineNumber);
                throw;
            }
        }

        public async Task<TResult> Run<TResult>(Func<Task<TResult>> function, [CallerMemberName] string origin = "",
            [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0)
        {
            try
            {
                return await Task.Run(function);
            }
            catch (Exception ex)
            {
                Logger.LogErrorSource(ex.ToString(), origin: origin, filePath: filePath, lineNumber: lineNumber);
                throw;
            }
        }

        public async Task<TResult> Run<TResult>(Func<TResult> function, CancellationToken cancellationToken,
            [CallerMemberName] string origin = "", [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0)
        {
            try
            {
                return await Task.Run(function, cancellationToken);
            }
            catch (Exception ex)
            {
                Logger.LogErrorSource(ex.ToString(), origin: origin, filePath: filePath, lineNumber: lineNumber);
                throw;
            }
        }

        public async Task<TResult> Run<TResult>(Func<TResult> function, [CallerMemberName] string origin = "",
            [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0)
        {
            try
            {
                return await Task.Run(function);
            }
            catch (Exception ex)
            {
                Logger.LogErrorSource(ex.ToString(), origin: origin, filePath: filePath, lineNumber: lineNumber);
                throw;
            }
        }

        public async Task Run(Func<Task> function, CancellationToken cancellationToken,
            [CallerMemberName] string origin = "", [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0)
        {
            try
            {
                await Task.Run(function, cancellationToken);
            }
            catch (Exception ex)
            {
                Logger.LogErrorSource(ex.ToString(), origin: origin, filePath: filePath, lineNumber: lineNumber);
                throw;
            }
        }

        public async void RunAndForget(Func<Task> function, CancellationToken cancellationToken,
            [CallerMemberName] string origin = "", [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0)
        {
            try
            {
                await Run(function, cancellationToken, origin, filePath, lineNumber);
            }
            catch
            {
                // ignored
            }
        }

        public async Task Run(Action action, CancellationToken cancellationToken, [CallerMemberName] string origin = "",
            [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0)
        {
            try
            {
                await Task.Run(action, cancellationToken);
            }
            catch (Exception ex)
            {
                Logger.LogErrorSource(ex.ToString(), origin: origin, filePath: filePath, lineNumber: lineNumber);
                throw;
            }
        }

        public async void RunAndForget(Action action, CancellationToken cancellationToken,
            [CallerMemberName] string origin = "", [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0)
        {
            try
            {
                await Run(action, cancellationToken, origin, filePath, lineNumber);
            }
            catch
            {
                // ignored
            }
        }

        public async Task Run(Action action, [CallerMemberName] string origin = "",
            [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0)
        {
            try
            {
                await Task.Run(action);
            }
            catch (Exception ex)
            {
                Logger.LogErrorSource(ex.ToString(), origin: origin, filePath: filePath, lineNumber: lineNumber);
                throw;
            }
        }

        public async void RunAndForget(Action action, [CallerMemberName] string origin = "",
            [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0)
        {
            try
            {
                await Run(action, origin, filePath, lineNumber);
            }
            catch
            {
                // ignored
            }
        }

        #endregion
    }
}