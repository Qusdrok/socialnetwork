﻿namespace SocialNetwork
{
    public enum ApplicationPage
    {
        None = 0,
        Login = 1,
        Chat = 2,
        About = 3,
        VirtualCoin = 4
    }

    public enum PageAnimation
    {
        None = 0,
        SlideAndFadeInFromRight = 1,
        SlideAndFadeOutToLeft = 2
    }
    
    public enum ElementHorizontalAlignment
    {
        Left = 0,
        Center = 1,
        Right = 2,
        Stretch = 3
    }
    
    public enum AnimationSlideInDirection
    {
        Left = 0,
        Right = 1,
        Top = 2,
        Bottom = 3
    }

    public enum IconType
    {
        None = 0,
        Picture = 1,
        File = 2
    }

    public enum MenuItemType
    {
        TextAndIcon = 0,
        Divider = 1,
        Header = 2
    }
}