﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace SocialNetwork
{
    public class TimeToDisplayConverter : BaseConverter<TimeToDisplayConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Debug.Assert(value != null, nameof(value) + " != null");
            var time = (DateTimeOffset) value;
            return time.ToLocalTime().ToString(time.Date == DateTimeOffset.UtcNow.Date ? "HH:mm" : "HH:mm, MMM yyyy");
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}