﻿using System;
using System.Globalization;

namespace SocialNetwork
{
    public class ListViewContainItemConverter : BaseConverter<ListViewContainItemConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new Random().Next(2)>1;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
