﻿using System;
using System.Globalization;
using System.Windows.Media;

namespace SocialNetwork
{
    public class BooleanToArrowColorConverter : BaseConverter<BooleanToArrowColorConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null && (bool)value ? Brushes.Black : Brushes.Transparent;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
