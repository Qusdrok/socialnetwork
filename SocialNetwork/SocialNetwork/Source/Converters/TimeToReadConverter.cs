﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace SocialNetwork
{
    public class TimeToReadConverter : BaseConverter<TimeToReadConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Debug.Assert(value != null, nameof(value) + " != null");
            var time = (DateTimeOffset) value;
            if (time == DateTimeOffset.MinValue) return string.Empty;
            return time.Date == DateTimeOffset.UtcNow.Date
                ? $"Read {time.ToLocalTime():HH:mm}"
                : $"Read {time.ToLocalTime():HH:mm, MMM yyyy}";
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}