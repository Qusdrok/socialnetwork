﻿using System;
using System.Globalization;
using System.Windows;

namespace SocialNetwork
{
    public class BooleanToVisibleCollapseConverter : BaseConverter<BooleanToVisibleCollapseConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter == null) return value != null && (bool) value ? Visibility.Visible : Visibility.Collapsed;
            return value != null && (bool) value ? Visibility.Collapsed : Visibility.Visible;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}