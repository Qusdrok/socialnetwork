﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows;

namespace SocialNetwork
{
    public class HorizontalAlignmentConverters : BaseConverter<HorizontalAlignmentConverters>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Debug.Assert(value != null, nameof(value) + " != null");
            return (HorizontalAlignment) value;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}