﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SocialNetwork
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        #region Protected Members

        private readonly object _propertyValueCheckLock = new object();

        #endregion

        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #region Command Helpers

        protected async Task RunCommandAsync(Expression<Func<bool>> flag, Func<Task> action)
        {
            lock (_propertyValueCheckLock)
            {
                if (flag.GetPropertyValue()) return;
                flag.SetPropertyValue(true);
            }

            try
            {
                await action();
            }
            finally
            {
                flag.SetPropertyValue(false);
            }
        }

        protected async Task<T> RunCommandAsync<T>(Expression<Func<bool>> flag, Func<Task<T>> action, T defaultValue = default)
        {
            lock (_propertyValueCheckLock)
            {
                if (flag.GetPropertyValue()) return defaultValue;
                flag.SetPropertyValue(true);
            }

            try
            {
                return await action();
            }
            finally
            {
                flag.SetPropertyValue(false);
            }
        }

        #endregion
    }
}
