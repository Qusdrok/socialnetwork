﻿using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SocialNetwork
{
    public class BaseDialogUserControl : UserControl
    {
        #region Private Members

        private readonly DialogWindow _dialogWindow;

        #endregion

        #region Public Commands

        public ICommand CloseCommand { get; private set; }

        #endregion

        #region Public Properties

        public int WindowMinimumWidth { get; set; } = 250;
        public int WindowMinimumHeight { get; set; } = 100;
        public int TitleHeight { get; set; } = 30;

        public string Title { get; set; }

        #endregion

        #region Constructor
        
        public BaseDialogUserControl()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            _dialogWindow = new DialogWindow();
            _dialogWindow.ViewModel = new DialogWindowViewModel(_dialogWindow);
            CloseCommand = new RelayCommand(() => _dialogWindow.Close());
        }

        #endregion

        #region Public Dialog Show Methods

        public Task ShowDialog<T>(T viewModel)
            where T : BaseDialogViewModel
        {
            var tcs = new TaskCompletionSource<bool>();
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    _dialogWindow.ViewModel.WindowMinimumWidth = WindowMinimumWidth;
                    _dialogWindow.ViewModel.WindowMinimumHeight = WindowMinimumHeight;
                    _dialogWindow.ViewModel.TitleHeight = TitleHeight;
                    _dialogWindow.ViewModel.Title = string.IsNullOrEmpty(viewModel.Title) ? Title : viewModel.Title;
                    _dialogWindow.ViewModel.Content = this;

                    DataContext = viewModel;
                    _dialogWindow.Owner = Application.Current.MainWindow;
                    _dialogWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    _dialogWindow.ShowDialog();
                }
                finally
                {
                    tcs.TrySetResult(true);
                }
            });

            return tcs.Task;
        }

        #endregion
    }
}