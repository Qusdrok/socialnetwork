﻿namespace SocialNetwork
{
    public abstract class BaseDialogViewModel : BaseViewModel
    {
        public string Title { get; set; }
    }
}