﻿using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Dna;

namespace SocialNetwork
{
    public class BasePage : UserControl
    {
        #region Private Member

        private object _vm;

        #endregion
        
        #region Public Properties
        
        public PageAnimation PageLoadAnimation { get; set; } = PageAnimation.SlideAndFadeInFromRight;
        
        public PageAnimation PageUnloadAnimation { get; set; } = PageAnimation.SlideAndFadeOutToLeft;
        
        public float SlideSeconds { get; set; } = 0.4f;
        
        public bool ShouldAnimateOut { get; set; }
        
        public object ViewModelObject
        {
            get => _vm;
            set
            {
                if (_vm == value) return;
                _vm = value;
                
                OnViewModelChanged();
                DataContext = _vm;
            }
        }

        #endregion

        #region Constructor

        public BasePage()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!PageLoadAnimation.Equals( PageAnimation.None)) Visibility = Visibility.Collapsed;
            Loaded += BasePage_LoadedAsync;
        }

        #endregion

        #region Animation Load / Unload

        private async void BasePage_LoadedAsync(object sender, RoutedEventArgs e)
        {
            if (ShouldAnimateOut)
            {
                await AnimateOutAsync();
            }
            else
            {
                await AnimateInAsync();
            }
        }
        
        public async Task AnimateInAsync()
        {
            if (PageLoadAnimation.Equals( PageAnimation.None)) return;
            switch (PageLoadAnimation)
            {
                case PageAnimation.SlideAndFadeInFromRight:
                    Debug.Assert(Application.Current.MainWindow != null, "Application.Current.MainWindow != null");
                    await this.SlideAndFadeInAsync(AnimationSlideInDirection.Right, false, SlideSeconds,
                        size: (int) Application.Current.MainWindow.Width);
                    break;
            }
        }

        public async Task AnimateOutAsync()
        {
            if (PageLoadAnimation.Equals( PageAnimation.None)) return;
            switch (PageUnloadAnimation)
            {
                case PageAnimation.SlideAndFadeOutToLeft:
                    await this.SlideAndFadeOutAsync(AnimationSlideInDirection.Left, SlideSeconds);
                    break;
            }
        }

        #endregion

        protected virtual void OnViewModelChanged()
        {
        }
    }

    public class BasePage<T> : BasePage
        where T : BaseViewModel, new()
    {
        #region Public Properties

        public T ViewModel
        {
            get => (T)ViewModelObject;
            set => ViewModelObject = value;
        }

        #endregion

        #region Constructor

        public BasePage()
        {
            if (DesignerProperties.GetIsInDesignMode(this))
            {
                ViewModel = new T();
            }
            else
            {
                ViewModel = Framework.Service<T>() ?? new T();
            }
        }

        public BasePage(T vm = default)
        {
            if (vm != null)
            {
                ViewModel = vm;
            }
            else
            {
                if (DesignerProperties.GetIsInDesignMode(this))
                {
                    ViewModel = new T();
                }
                else
                {
                    ViewModel = Framework.Service<T>() ?? new T();
                }
            }
        }
        
        #endregion
    }
}