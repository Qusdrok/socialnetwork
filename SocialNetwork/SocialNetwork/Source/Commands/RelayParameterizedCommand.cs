﻿using System;
using System.Windows.Input;

namespace SocialNetwork
{
    public class RelayParameterCommand : ICommand
    {
        #region Private Members

        private readonly Action<object> _action;

        #endregion

        #region Public Events

        public event EventHandler CanExecuteChanged = (sender, e) => { };

        #endregion

        #region Constructor

        public RelayParameterCommand(Action<object> action)
        {
            _action = action;
        }

        #endregion

        #region Command Methods

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _action(parameter);
        }

        #endregion
    }
}
