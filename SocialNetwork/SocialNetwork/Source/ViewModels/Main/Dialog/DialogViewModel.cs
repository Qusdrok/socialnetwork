﻿namespace SocialNetwork
{
    public class DialogViewModel : BaseDialogViewModel
    {
        public string Message { get; set; }

        public string OkText { get; set; } = "OK";
    }
}