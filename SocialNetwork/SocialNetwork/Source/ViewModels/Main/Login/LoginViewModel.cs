﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace SocialNetwork
{
    public class LoginViewModel : BaseViewModel
    {
        #region Public Properties

        public bool IsSigning { get; set; }

        #endregion

        #region Commands

        public ICommand LoginCommand { get; set; }

        #endregion

        #region Constructor

        public LoginViewModel()
        {
            LoginCommand = new RelayParameterCommand(async parameter => await LoginAsync(parameter));
        }

        #endregion

        private async Task LoginAsync(object parameter)
        {
            await RunCommandAsync(() => IsSigning, async () =>
            {
                await Task.Delay(5000);
            });
        }
    }
}