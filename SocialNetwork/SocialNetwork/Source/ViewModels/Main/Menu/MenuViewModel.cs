﻿using System.Collections.Generic;

namespace SocialNetwork
{
    public class MenuViewModel : BaseViewModel
    {
        public List<MenuItemViewModel> Items { get; set; }
    }
}