﻿using System;
using System.Security;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SocialNetwork
{
    public class PasswordEntryViewModel : BaseViewModel
    {
        #region Public Properties

        public string Label { get; set; }
        public string FakePassword { get; set; }
        public string CurrentPasswordHintText { get; set; }
        public string NewPasswordHintText { get; set; }
        public string ConfirmPasswordHintText { get; set; }

        public SecureString CurrentPassword { get; set; }
        public SecureString NewPassword { get; set; }
        public SecureString ConfirmPassword { get; set; }

        public bool Editing { get; set; }
        public bool Working { get; set; }

        public Func<Task<bool>> CommitAction { get; set; }

        #endregion

        #region Public Commands

        public ICommand EditCommand { get; set; }
        public ICommand CancelCommand { get; set; }
        public ICommand SaveCommand { get; set; }

        #endregion

        #region Constructor

        public PasswordEntryViewModel()
        {
            EditCommand = new RelayCommand(Edit);
            CancelCommand = new RelayCommand(Cancel);
            SaveCommand = new RelayCommand(Save);

            CurrentPasswordHintText = "Current Password";
            NewPasswordHintText = "New Password";
            ConfirmPasswordHintText = "Confirm Password";
        }

        #endregion

        #region Command Methods

        public void Edit()
        {
            NewPassword = new SecureString();
            ConfirmPassword = new SecureString();
            Editing = true;
        }

        public void Cancel()
        {
            Editing = false;
        }

        public void Save()
        {
            var result = default(bool);
            RunCommandAsync(() => Working, async () =>
            {
                Editing = false;
                result = CommitAction == null || await CommitAction();
            }).ContinueWith(t =>
            {
                if (!result)
                {
                    Editing = true;
                }
            });
        }

        #endregion
    }
}