﻿using System.Collections.Generic;
using System.Windows.Input;

namespace SocialNetwork
{
    public struct Screen
    {
        public BaseViewModel ViewModel { get; set; }
        public ApplicationPage Page { get; set; }
    }

    public class DropdownMenuItemViewModel : BaseViewModel
    {
        #region Public Member

        public List<DropdownMenuItemViewModel> SubItem { get; set; }

        public DropdownMenuItemViewModel SelectedItem { get; set; }

        public Screen Screen { get; set; }

        public string Icon { get; set; }
        
        public string Name { get; set; }

        #endregion

        #region Public Commands
        #endregion

        #region Constructor

        public DropdownMenuItemViewModel()
        {
        }

        #endregion

        #region Command Methods

        public void SwitchScreen(DropdownMenuItemViewModel item)
        {
            var sr = DropdownMenuModel.Instance.GetScreen(item);
            DI.ApplicationVM.GoToPage(sr.Page, sr.ViewModel);
        }

        #endregion
    }
}