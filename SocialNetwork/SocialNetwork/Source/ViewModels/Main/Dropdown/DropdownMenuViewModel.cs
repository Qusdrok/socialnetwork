﻿using System.Collections.Generic;

namespace SocialNetwork
{
    public class DropdownMenuViewModel : BaseViewModel
    {
        public List<DropdownMenuItemViewModel> Items { get; set; }
    }
}