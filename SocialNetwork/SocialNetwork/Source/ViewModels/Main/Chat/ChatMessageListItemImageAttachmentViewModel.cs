﻿using System.Threading.Tasks;

namespace SocialNetwork
{
    public class ChatMessageListItemImageAttachmentViewModel : BaseViewModel
    {
        #region Private Members

        private string _thumbnailUrl;

        #endregion

        public string Title { get; set; }

        public string FileName { get; set; }

        public long FileSize { get; set; }

        public string ThumbnailUrl
        {
            get => _thumbnailUrl;
            set
            {
                if (value == _thumbnailUrl) return;
                _thumbnailUrl = value;
                
                Task.Delay(2000).ContinueWith(t =>
                    LocalFilePath = "/Resources/Assets/Bgs/rusty.jpg");
            }
        }

        /// <summary>
        /// The local file path on this machine to the downloaded thumbnail
        /// </summary>
        public string LocalFilePath { get; set; }

        /// <summary>
        /// Indicates if an image has loaded
        /// </summary>
        public bool ImageLoaded => LocalFilePath != null;
    }
}