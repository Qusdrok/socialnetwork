﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace SocialNetwork
{
    public class AboutViewModel : BaseViewModel
    {
        #region Constant

        private const string FB_URL = "https://www.facebook.com/qusdrok/";
        private const string WEBSITE_URL = "https://qusdroks.000webhostapp.com/";

        #endregion

        #region Public Member

        public double RestHeight { get; set; }
        public double RestWidth { get; set; }

        #endregion

        #region Command

        public ICommand OpenMyFBCommand { get; set; }
        public ICommand OpenMyWebsiteCommand { get; set; }

        #endregion

        #region Constructor

        public AboutViewModel()
        {
            var width = SystemParameters.WorkArea.Width;
            var height = SystemParameters.WorkArea.Height;

            RestWidth = width - 250;
            RestHeight = height;

            OpenMyFBCommand = new RelayCommand(OpenMyFB);
            OpenMyWebsiteCommand = new RelayCommand(OpenMyWebsite);
        }

        #endregion

        #region Public Function

        public void OpenMyFB()
        {
            Process.Start(FB_URL);
        }

        public void OpenMyWebsite()
        {
            Process.Start(WEBSITE_URL);
        }

        #endregion
    }
}