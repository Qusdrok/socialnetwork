﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;

namespace SocialNetwork
{
    public class WindowViewModel : BaseViewModel
    {
        #region Private Member

        private const int WM_NCLBUTTONDBLCLK = 0x00A3;
        private const int WM_SYSCOMMAND = 0x0112;
        private const int SC_MOVE = 0xF010;

        private readonly Window _window;
        private readonly WindowResizerHelpers _windowResizer;

        #endregion

        #region Public Properties

        public double WindowMinimumWidth { get; set; } = 800;
        public double WindowMinimumHeight { get; set; } = 500;

        public bool DimmableOverlayVisible { get; set; }

        public int TitleHeight { get; set; } = 45;

        public Thickness ResizeBorderThickness => new Thickness(5);

        public Thickness InnerContentPadding { get; set; } = new Thickness(0);

        public GridLength TitleHeightGridLength => new GridLength(TitleHeight);

        #endregion

        #region Commands

        public ICommand MinimizeCommand { get; set; }
        public ICommand CloseCommand { get; set; }
        public ICommand MenuCommand { get; set; }

        #endregion

        #region Constructor

        public WindowViewModel(Window window)
        {
            _window = window;
            _window.StateChanged += (sender, e) => OnPropertyChanged(nameof(ResizeBorderThickness));

            MinimizeCommand = new RelayCommand(() => _window.WindowState = WindowState.Minimized);
            CloseCommand = new RelayCommand(() => _window.Close());
        }

        #endregion

        #region Private Helpers

        private Point GetMousePosition()
        {
            return _windowResizer.GetCursorPosition();
        }

        public void WindowProc(object sender, EventArgs e)
        {
            var source = HwndSource.FromHwnd(new WindowInteropHelper(_window).Handle);
            source?.AddHook(WndProc);
        }

        private static IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                // Disable double click in title bar
                case WM_NCLBUTTONDBLCLK:
                    handled = true;
                    break;

                // Disable drag and move window
                case WM_SYSCOMMAND:
                    var command = wParam.ToInt32() & 0xfff0;
                    if (command.Equals(SC_MOVE)) handled = true;
                    break;
            }

            return IntPtr.Zero;
        }

        #endregion
    }
}