﻿namespace SocialNetwork
{
    public class LocatorViewModel
    {
        public static LocatorViewModel Instance { get; private set; } = new LocatorViewModel();
        public static ApplicationViewModel ApplicationViewModel => DI.ApplicationVM;
        public static VirtualCoinSettingViewModel VCSettingVM => DI.VCSettingVM;
    }
}