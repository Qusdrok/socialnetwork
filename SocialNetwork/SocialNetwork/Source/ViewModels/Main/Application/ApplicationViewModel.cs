﻿namespace SocialNetwork
{
    public class ApplicationViewModel : BaseViewModel
    {
        public ApplicationPage CurrentPage { get; private set; }

        public BaseViewModel CurrentPageViewModel { get; set; }

        public bool ServerReachable { get; set; } = true;

        public void GoToPage(ApplicationPage page, BaseViewModel viewModel = null)
        {
            CurrentPageViewModel = viewModel;
            CurrentPage = page;
            OnPropertyChanged(nameof(CurrentPage));
        }
    }
}