﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace SocialNetwork
{
    public class SettingsViewModel : BaseViewModel
    {
        #region Private Members

        private const string LoadingText = "...";

        #endregion

        #region Public Properties

        public TextEntryViewModel FirstName { get; set; }
        public TextEntryViewModel LastName { get; set; }
        public TextEntryViewModel Username { get; set; }
        public PasswordEntryViewModel Password { get; set; }
        public TextEntryViewModel Email { get; set; }
        public string LogoutButtonText { get; set; }

        #region Transactional Properties

        public bool FirstNameIsSaving { get; set; }
        public bool LastNameIsSaving { get; set; }
        public bool UsernameIsSaving { get; set; }
        public bool EmailIsSaving { get; set; }
        public bool PasswordIsChanging { get; set; }
        public bool SettingsLoading { get; set; }
        public bool LoggingOut { get; set; }

        #endregion

        #endregion

        #region Public Commands

        public ICommand OpenCommand { get; set; }
        public ICommand CloseCommand { get; set; }
        public ICommand LogoutCommand { get; set; }
        public ICommand ClearUserDataCommand { get; set; }
        public ICommand LoadCommand { get; set; }
        public ICommand SaveFirstNameCommand { get; set; }
        public ICommand SaveLastNameCommand { get; set; }
        public ICommand SaveUsernameCommand { get; set; }
        public ICommand SaveEmailCommand { get; set; }

        #endregion

        #region Constructor

        public SettingsViewModel()
        {
            FirstName = new TextEntryViewModel
            {
                Label = "First Name",
                OriginalText = LoadingText,
                /*CommitAction = SaveFirstNameAsync*/
            };

            LastName = new TextEntryViewModel
            {
                Label = "Last Name",
                OriginalText = LoadingText,
                /*CommitAction = SaveLastNameAsync*/
            };

            Username = new TextEntryViewModel
            {
                Label = "Username",
                OriginalText = LoadingText,
                /*CommitAction = SaveUsernameAsync*/
            };

            Password = new PasswordEntryViewModel
            {
                Label = "Password",
                FakePassword = "********",
                /*CommitAction = SavePasswordAsync*/
            };

            Email = new TextEntryViewModel
            {
                Label = "Email",
                OriginalText = LoadingText,
                /*CommitAction = SaveEmailAsync*/
            };

            OpenCommand = new RelayCommand(Open);
            CloseCommand = new RelayCommand(Close);
            LogoutCommand = new RelayCommand(async () => await LogoutAsync());
            ClearUserDataCommand = new RelayCommand(ClearUserData);
            /*LoadCommand = new RelayCommand(async () => await LoadAsync());
            SaveFirstNameCommand = new RelayCommand(async () => await SaveFirstNameAsync());
            SaveLastNameCommand = new RelayCommand(async () => await SaveLastNameAsync());
            SaveUsernameCommand = new RelayCommand(async () => await SaveUsernameAsync());
            SaveEmailCommand = new RelayCommand(async () => await SaveEmailAsync());*/
            LogoutButtonText = "Logout";
        }

        #endregion

        #region Command Methods

        public void Open()
        {
            /*IoC.Application.SettingsMenuVisible = true;*/
        }

        public void Close()
        {
            /*IoC.Application.SettingsMenuVisible = false;*/
        }

        public async Task LogoutAsync()
        {
            await RunCommandAsync(() => LoggingOut, async () =>
            {
                /*await ClientDataStore.ClearAllLoginCredentialsAsync();*/
                ClearUserData();
                /*ViewModelApplication.GoToPage(ApplicationPage.Login);*/
            });
        }

        public void ClearUserData()
        {
            FirstName.OriginalText = LoadingText;
            LastName.OriginalText = LoadingText;
            Username.OriginalText = LoadingText;
            Email.OriginalText = LoadingText;
        }

        /*public async Task LoadAsync()
        {
            await RunCommandAsync(() => SettingsLoading, async () =>
            {
                var scopedClientDataStore = ClientDataStore;
                await UpdateValuesFromLocalStoreAsync(scopedClientDataStore);
                
                var token = (await scopedClientDataStore.GetLoginCredentialsAsync())?.Token;
                if (string.IsNullOrEmpty(token)) return;
                
                var result = await WebRequests.PostAsync<ApiResponse<UserProfileDetailsApiModel>>(
                    RouteHelpers.GetAbsoluteRoute(ApiRoutes.GetUserProfile),
                    bearerToken: token);
                if (await result.HandleErrorIfFailedAsync("Load User Details Failed")) return;
                
                var dataModel = result.ServerResponse.Response.ToLoginCredentialsDataModel();
                dataModel.Token = token;
                
                await scopedClientDataStore.SaveLoginCredentialsAsync(dataModel);
                await UpdateValuesFromLocalStoreAsync(scopedClientDataStore);
            });
        }*/

        /*public async Task<bool> SaveFirstNameAsync()
        {
            return await RunCommandAsync(() => FirstNameIsSaving, async () =>
            {
                return await UpdateUserCredentialsValueAsync(
                    "First Name",
                    credentials => credentials.FirstName,
                    FirstName.OriginalText,
                    (apiModel, value) => apiModel.FirstName = value);
            });
        }*/
        
        /*public async Task<bool> SaveLastNameAsync()
        {
            return await RunCommandAsync(() => LastNameIsSaving, async () =>
            {
                return await UpdateUserCredentialsValueAsync(
                    "Last Name",
                    credentials => credentials.LastName,
                    LastName.OriginalText,
                    (apiModel, value) => apiModel.LastName = value);
            });
        }*/

        /*public async Task<bool> SaveUsernameAsync()
        {
            return await RunCommandAsync(() => UsernameIsSaving, async () =>
            {
                return await UpdateUserCredentialsValueAsync(
                    "Username",
                    credentials => credentials.Username,
                    Username.OriginalText,
                    (apiModel, value) => apiModel.Username = value);
            });
        }*/
        
        /*public async Task<bool> SaveEmailAsync()
        {
            return await RunCommandAsync(() => EmailIsSaving, async () =>
            {
                return await UpdateUserCredentialsValueAsync(
                    "Email",
                    credentials => credentials.Email,
                    Email.OriginalText,
                    (apiModel, value) => apiModel.Email = value);
            });
        }*/
        
        /*public async Task<bool> SavePasswordAsync()
        {
            return await RunCommandAsync(() => PasswordIsChanging, async () =>
            {
                Logger.LogDebugSource($"Changing password...");
                var credentials = await ClientDataStore.GetLoginCredentialsAsync();

                if (Password.NewPassword.Unsecure() != Password.ConfirmPassword.Unsecure())
                {
                    await UI.ShowMessage(new DialogViewModel
                    {
                        Title = "Password Mismatch",
                        Message = "New password and confirm password must match"
                    });

                    return false;
                }

                var result = await WebRequests.PostAsync<ApiResponse>(
                    RouteHelpers.GetAbsoluteRoute(ApiRoutes.UpdateUserPassword),
                    new UpdateUserPasswordApiModel
                    {
                        CurrentPassword = Password.CurrentPassword.Unsecure(),
                        NewPassword = Password.NewPassword.Unsecure()
                    }, 
                    bearerToken: credentials.Token);

                if (await result.HandleErrorIfFailedAsync($"Change Password"))
                {
                    Logger.LogDebugSource($"Failed to change password. {result.ErrorMessage}");
                    return false;
                }

                Logger.LogDebugSource($"Successfully changed password");
                return true;
            });
        }*/

        #endregion

        #region Private Helper Methods
        
        /*private async Task UpdateValuesFromLocalStoreAsync(IClientDataStore clientDataStore)
        {
            var storedCredentials = await clientDataStore.GetLoginCredentialsAsync();
            FirstName.OriginalText = storedCredentials?.FirstName;
            LastName.OriginalText = storedCredentials?.LastName;
            Username.OriginalText = storedCredentials?.Username;
            Email.OriginalText = storedCredentials?.Email;
        }*/
        
        /*private async Task<bool> UpdateUserCredentialsValueAsync(string displayName, Expression<Func<LoginCredentialsDataModel, string>> propertyToUpdate, string newValue, Action<UpdateUserProfileApiModel, string> setApiModel)
        {
            Logger.LogDebugSource($"Saving {displayName}...");
            var credentials = await ClientDataStore.GetLoginCredentialsAsync();
            var toUpdate = propertyToUpdate.GetPropertyValue(credentials);

            Logger.LogDebugSource($"{displayName} currently {toUpdate}, updating to {newValue}");
            if (toUpdate == newValue)
            {
                Logger.LogDebugSource($"{displayName} is the same, ignoring");
                return true;
            }

            propertyToUpdate.SetPropertyValue(newValue, credentials);
            var updateApiModel = new UpdateUserProfileApiModel();
            
            setApiModel(updateApiModel, newValue);
            var result = await WebRequests.PostAsync<ApiResponse>(
                RouteHelpers.GetAbsoluteRoute(ApiRoutes.UpdateUserProfile),
                updateApiModel,
                bearerToken: credentials.Token);

            if (await result.HandleErrorIfFailedAsync($"Update {displayName}"))
            {
                Logger.LogDebugSource($"Failed to update {displayName}. {result.ErrorMessage}");
                return false;
            }

            Logger.LogDebugSource($"Successfully updated {displayName}. Saving to local database cache...");
            await ClientDataStore.SaveLoginCredentialsAsync(credentials);
            return true;
        }*/

        #endregion
    }
}