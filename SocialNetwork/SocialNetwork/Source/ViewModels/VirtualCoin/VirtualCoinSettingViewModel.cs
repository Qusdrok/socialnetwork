﻿using System.Windows.Input;

namespace SocialNetwork
{
    public class VirtualCoinSettingViewModel : BaseViewModel
    {
        #region Private Member

        private bool _isOpenSetting;

        #endregion

        #region Public Member

        public bool IsOpenSetting
        {
            get => _isOpenSetting;
            set
            {
                if (!_isOpenSetting.Equals(value))
                {
                    _isOpenSetting = value;
                }
            }
        }

        #endregion

        #region Command

        public ICommand OpenSettingCommand { get; set; }
        public ICommand CloseSettingCommand { get; set; }

        #endregion

        #region Constructor

        public VirtualCoinSettingViewModel()
        {
            OpenSettingCommand = new RelayCommand(OpenSetting);
            CloseSettingCommand = new RelayCommand(CloseSetting);
        }

        #endregion

        #region Private Helper

        private void OpenSetting()
        {
            IsOpenSetting = true;
        }

        private void CloseSetting()
        {
            IsOpenSetting = false;
        }

        #endregion
    }
}
