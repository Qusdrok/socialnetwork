﻿using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Win32;

namespace SocialNetwork
{
    public class VirtualCoinViewModel : BaseViewModel
    {
        #region Private Member

        private readonly char[] _splits = {']', '[', ',', '|', ';', ':'};
        private string _totalAccount = "Total Account: 0";
        private string _totalThread = "Total Thread: 0";
        private string _totalBet = "Total Bet: 0";
        private string _totalWin = "Total Win: 0";
        private string _totalLose = "Total Lose: 0";

        #endregion

        #region Public Member

        public ObservableCollection<AccountModel> Accounts { get; set; } = new ObservableCollection<AccountModel>();

        public string TotalAccount
        {
            get => _totalAccount;
            set
            {
                if (_totalAccount.Equals(value)) return;
                _totalAccount = value;
                OnPropertyChanged(nameof(TotalAccount));
            }
        }
        
        public string TotalThread
        {
            get => _totalThread;
            set
            {
                if (_totalThread.Equals(value)) return;
                _totalThread = value;
                OnPropertyChanged(nameof(TotalThread));
            }
        }
        
        public string TotalBet
        {
            get => _totalBet;
            set
            {
                if (_totalBet.Equals(value)) return;
                _totalBet = value;
                OnPropertyChanged(nameof(TotalBet));
            }
        }
        
        public string TotalWin
        {
            get => _totalWin;
            set
            {
                if (_totalWin.Equals(value)) return;
                _totalWin = value;
                OnPropertyChanged(nameof(TotalWin));
            }
        }
        
        public string TotalLose
        {
            get => _totalLose;
            set
            {
                if (_totalLose.Equals(value)) return;
                _totalLose = value;
                OnPropertyChanged(nameof(TotalLose));
            }
        }

        public double RestHeight { get; set; }
        public double RestWidth { get; set; }

        #endregion

        #region Command

        public ICommand AddAccountCommand { get; set; }
        public ICommand RemoveAccountCommand { get; set; }
        public ICommand StartCommand { get; set; }
        public ICommand StopCommand { get; set; }

        #endregion

        #region Constructor

        public VirtualCoinViewModel()
        {
            var width = SystemParameters.WorkArea.Width;
            var height = SystemParameters.WorkArea.Height;

            RestWidth = width - 250;
            RestHeight = height - 47;
            
            AddAccountCommand = new RelayCommand(AddAccount);
            RemoveAccountCommand = new RelayParameterCommand(RemoveAccount);
            StartCommand = new RelayCommand(Start);
            StopCommand = new RelayCommand(Stop);
        }

        #endregion

        #region Private Function

        private async void AddAccount()
        {
            var dialog = new OpenFileDialog
            {
                Filter = "Text|*.txt"
            };

            if (!(dialog.ShowDialog() ?? false)) return;
            var fileName = dialog.FileName;

            if (string.IsNullOrEmpty(fileName) || !File.Exists(fileName)) return;
            var lines = File.ReadAllLines(dialog.FileName);

            foreach (var x in lines)
            {
                var data = x.Split(_splits);
                if (Accounts.Count > 0)
                {
                    foreach (var y in Accounts.ToList())
                    {
                        if (y.Username.Equals(data[0]) && y.Password.Equals(data[1]))
                        {
                            await DI.UI.ShowMessage(new DialogViewModel
                            {
                                Title = "Thông báo",
                                Message = $"{data[0]}, {data[1]} was exists"
                            });

                            break;
                        }

                        Accounts.Add(new AccountModel(data[0], data[1]));
                    }
                }
                else
                {
                    Accounts.Add(new AccountModel(data[0], data[1]));
                }
            }

            UpdateTotalAccount();
        }

        private void RemoveAccount(object dg)
        {
            var dgv = (DataGrid)dg;
            if (dgv == null) return;

            Accounts.Remove((AccountModel)dgv.SelectedItem);
            dgv.ItemsSource = Accounts;
            UpdateTotalAccount();
        }

        private void Start()
        {
        }

        private void Stop()
        {
        }

        private void UpdateTotalAccount()
        {
            TotalAccount = $"Total Accounts: {Accounts.Count}";
        }

        #endregion
    }
}