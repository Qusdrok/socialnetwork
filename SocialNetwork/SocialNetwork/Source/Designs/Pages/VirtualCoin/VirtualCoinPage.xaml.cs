﻿namespace SocialNetwork
{
    public partial class VirtualCoinPage : BasePage<VirtualCoinViewModel>
    {
        public VirtualCoinPage()
        {
            InitializeComponent();
        }

        public VirtualCoinPage(VirtualCoinViewModel vm) : base(vm)
        {
            InitializeComponent();
            DataContext = new VirtualCoinViewModel();
        }
    }
}