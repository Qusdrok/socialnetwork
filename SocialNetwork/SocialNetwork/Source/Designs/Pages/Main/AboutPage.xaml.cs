﻿namespace SocialNetwork
{
    public partial class AboutPage : BasePage<AboutViewModel>
    {
        public AboutPage()
        {
            InitializeComponent();
        }

        public AboutPage(AboutViewModel vm) : base(vm)
        {
            InitializeComponent();
        }
    }
}