﻿using System.Security;

namespace SocialNetwork
{
    public partial class LoginPage : BasePage<LoginViewModel>, IPassword
    {
        public LoginPage()
        {
            InitializeComponent();
        }
        
        public LoginPage(LoginViewModel vm) : base(vm)
        {
            InitializeComponent();
        }

        public SecureString SecurePassword => PasswordText.SecurePassword;
    }
}
