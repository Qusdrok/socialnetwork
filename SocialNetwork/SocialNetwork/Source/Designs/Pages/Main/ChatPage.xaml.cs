﻿using System;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace SocialNetwork
{
    public partial class ChatPage : BasePage<ChatMessageListViewModel>
    {
        public ChatPage()
        {
            InitializeComponent();
        }
        
        public ChatPage(ChatMessageListViewModel vm) : base(vm)
        {
            InitializeComponent();
        }

        #region Override Methods

        protected override void OnViewModelChanged()
        {
            if (ChatMessageList == null) return;
            var sb = new Storyboard();
            
            sb.AddFadeIn(1);
            sb.Begin(ChatMessageList);
            MessageText.Focus();
        }

        #endregion
        
        private void MessageText_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var tb = sender as TextBox;
            if (e.Key != Key.Enter) return;
            if (Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
            {
                Debug.Assert(tb != null, nameof(tb) + " != null");
                var index = tb.CaretIndex;
                
                tb.Text = tb.Text.Insert(index, Environment.NewLine);
                tb.CaretIndex = index + Environment.NewLine.Length;
                e.Handled = true;
            }
            else
            {
                ViewModel.Send();
            }

            e.Handled = true;
        }
    }
}