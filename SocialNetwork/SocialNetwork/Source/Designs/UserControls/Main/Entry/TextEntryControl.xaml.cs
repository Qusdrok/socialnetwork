﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace SocialNetwork
{
    public partial class TextEntryControl : UserControl
    {
        #region Dependency Properties

        public GridLength LabelWidth
        {
            get => (GridLength) GetValue(LabelWidthProperty);
            set => SetValue(LabelWidthProperty, value);
        }

        public static readonly DependencyProperty LabelWidthProperty =
            DependencyProperty.Register("LabelWidth", typeof(GridLength), typeof(TextEntryControl),
                new PropertyMetadata(GridLength.Auto, LabelWidthChangedCallback));

        #endregion

        #region Constructor

        public TextEntryControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Dependency Callbacks

        public static void LabelWidthChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                ((TextEntryControl) d).LabelColumnDefinition.Width = (GridLength) e.NewValue;
            }
            catch (Exception)
            {
                Debugger.Break();
                ((TextEntryControl) d).LabelColumnDefinition.Width = GridLength.Auto;
            }
        }

        #endregion
    }
}