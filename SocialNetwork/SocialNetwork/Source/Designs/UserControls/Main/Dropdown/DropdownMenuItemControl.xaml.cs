﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace SocialNetwork
{
    public partial class DropdownMenuItemControl : UserControl
    {
        #region Dependency Properties

        public Thickness SubItemPadding
        {
            get => (Thickness)GetValue(SubItemPaddingProperty);
            set => SetValue(SubItemPaddingProperty, value);
        }

        public static readonly DependencyProperty SubItemPaddingProperty =
            DependencyProperty.Register(nameof(SubItemPadding),
                typeof(Thickness), typeof(DropdownMenuItemControl));

        #endregion

        public DropdownMenuItemControl()
        {
            InitializeComponent();
        }

        private void ListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var lv = (ListView) sender;
            if (lv == null) return;

            var item = (DropdownMenuItemViewModel) lv.SelectedItem;
            item.SwitchScreen(item);
        }
    }
}