﻿using System.Windows.Controls;

namespace SocialNetwork
{
    public partial class DropdownMenuControl : UserControl
    {
        public DropdownMenuControl()
        {
            InitializeComponent();
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            var sv = (ScrollViewer)sender;
            sv.ScrollToVerticalOffset(sv.VerticalOffset - e.Delta);
            e.Handled = true;
        }
    }
}