﻿using System;

namespace SocialNetwork
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            var window = new WindowViewModel(this);
            SourceInitialized += window.WindowProc;
            DataContext = window;
        }

        private void AppWindow_Deactivated(object sender, EventArgs e)
        {
            ((WindowViewModel) DataContext).DimmableOverlayVisible = true;
        }

        private void AppWindow_Activated(object sender, EventArgs e)
        {
            ((WindowViewModel) DataContext).DimmableOverlayVisible = false;
        }
    }
}